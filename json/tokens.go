package json

import "gitlab.com/gascoigne/pogo"

const (
	INT_LITERAL    = pogo.TokenType("INT_LITERAL")
	STRING_LITERAL = pogo.TokenType("STRING_LITERAL")
	BOOL_LITERAL   = pogo.TokenType("BOOL_LITERAL")
)
