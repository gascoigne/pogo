package main

import (
	"flag"

	"gitlab.com/gascoigne/pogo"
	_ "gitlab.com/gascoigne/pogo/json"
)

var path = flag.String("path", "", "")
var pkg = flag.String("pkg", "", "")

func main() {
	flag.Parse()
	pogo.VisitorTemplate.Generate("json", *pkg, *path)
}
