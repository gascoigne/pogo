package pogo

import (
	"bytes"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"reflect"
	"strings"

	"golang.org/x/tools/imports"
)

var (
	generateVisitor = flag.Bool("generate-visitor", false, "")
	public          = flag.Bool("public", false, "")
)

var VisitorTemplate = visitorTemplate{
	Productions: make(map[string]reflect.Type),
}

type visitorTemplate struct {
	Productions     map[string]reflect.Type
	ProductionOrder []string
	EmbedInterface  string
}

func packageShortName(path string) string {
	parts := strings.Split(path, "/")
	return parts[len(parts)-1]
}

var prodTemplate = template.Must(template.New("").Funcs(funcMap).Parse(`// {{ xid "all" }}{{.Prod}}s accepts all {{.Prod}} productions in the given list of items
func {{ xid "all" }}{{.Prod}}s(v {{.PkgName}}Visitor, items []pogo.Parsed) []{{.TypeName}} {
	result := make([]{{.TypeName}}, 0)
	for _, p := range items {
		if _, ok := {{ xid "is" }}{{.Prod}}(p); ok {
			result = append(result, {{ xid "accept" }}{{.Prod}}(v, p))
		}
	}
	return result
}

// {{ xid "accept" }}{{.Prod}} visits p as a {{.Prod}} production
func {{ xid "accept" }}{{.Prod}}(v {{.PkgName}}Visitor, p pogo.Parsed) {{.TypeName}} {
	if parseDebug {
		fmt.Printf("{{ xid "accept" }}{{.Prod}}: %%v\n", p)
	}

	if prod, ok := {{ xid "is" }}{{.Prod}}(p); ok {
		return v.{{ xid "visit" }}{{.Prod}}(v, []pogo.Parsed(prod.Children))
	}

	panic("expected {{.Prod}}")
}

// {{ xid "build" }}{{.Prod}}Visitor constructs a {{.PkgName}}Visitor which overrides visit{{.Prod}} on the base visitor
func {{ xid "build" }}{{.Prod}}Visitor(base {{.PkgName}}Visitor, fn func(delegate {{.PkgName}}Visitor, items []pogo.Parsed) {{.TypeName}}) {{.PkgName}}Visitor {
	return &{{.PkgName}}LambdaVisitor{
		base: base,
		{{ xid "visit" }}{{.Prod}}Func: fn,
{{if .EmbedInterface}}{{.EmbedInterface}}: base.getEmbedded(),{{end}}
	}
}

func (v *{{.PkgName}}LambdaVisitor) {{ xid "visit" }}{{.Prod}}(delegate {{.PkgName}}Visitor, items []pogo.Parsed) {{.TypeName}} {
	if v.{{ xid "visit" }}{{.Prod}}Func != nil {
		return v.{{ xid "visit" }}{{.Prod}}Func(delegate, items)
	}

	return v.base.{{ xid "visit" }}{{.Prod}}(delegate, items)
}
`))

var untypedProdTemplate = template.Must(template.New("").Funcs(funcMap).Parse(`// {{ xid "all" }}{{.Prod}}s accepts all {{.Prod}} productions in the given list of items
func {{ xid "all" }}{{.Prod}}s(v {{.PkgName}}Visitor, items []pogo.Parsed) {
	for _, p := range items {
		if _, ok := {{ xid "is" }}{{.Prod}}(p); ok {
			{{ xid "accept" }}{{.Prod}}(v, p)
		}
	}
}

// {{ xid "accept" }}{{.Prod}} visits p as a {{.Prod}} production
func {{ xid "accept" }}{{.Prod}}(v {{.PkgName}}Visitor, p pogo.Parsed){
	if parseDebug {
		fmt.Printf("{{ xid "accept" }}{{.Prod}}: %%v\n", p)
	}

	if prod, ok := {{ xid "is" }}{{.Prod}}(p); ok {
		v.{{ xid "visit" }}{{.Prod}}(v, []pogo.Parsed(prod.Children))
		return
	}

	panic("expected {{.Prod}}")
}

// {{ xid "build" }}{{.Prod}}Visitor constructs a {{.PkgName}}Visitor which overrides visit{{.Prod}} on the base visitor
func {{ xid "build" }}{{.Prod}}Visitor(base {{.PkgName}}Visitor, fn func(delegate {{.PkgName}}Visitor, items []pogo.Parsed)) {{.PkgName}}Visitor {
	return &{{.PkgName}}LambdaVisitor{
		base: base,
		{{ xid "visit" }}{{.Prod}}Func: fn,
{{if .EmbedInterface}}{{.EmbedInterface}}: base.getEmbedded(),{{end}}
	}
}

func (v *{{.PkgName}}LambdaVisitor) {{ xid "visit" }}{{.Prod}}(delegate {{.PkgName}}Visitor, items []pogo.Parsed) {
	if v.{{ xid "visit" }}{{.Prod}}Func != nil {
		v.{{ xid "visit" }}{{.Prod}}Func(delegate, items)
		return
	}

	v.base.{{ xid "visit" }}{{.Prod}}(delegate, items)
}

`))

var commonProdTemplate = template.Must(template.New("").Funcs(funcMap).Parse(`// {{ xid "is" }}{{.Prod}} determines if p is a {{.Prod}} production
func {{ xid "is" }}{{.Prod}}(p pogo.Parsed) (pogo.Production, bool) {
	if prod, ok := p.(pogo.Production); ok {
		if prod.Ident == "{{.Prod}}" {
			return prod, ok
		}
	}
	return pogo.Production{}, false
}

// {{ xid "all" }}{{.Prod}}Children finds all {{.Prod}} productions in the given list of items, and returns their children as one list
func {{ xid "all" }}{{.Prod}}Children(v {{.PkgName}}Visitor, items []pogo.Parsed) []pogo.Parsed {
	result := make([]pogo.Parsed, 0)
	for _, p := range items {
		if prod, ok := {{ xid "is" }}{{.Prod}}(p); ok {
			result = append(result, prod.Children...)
		}
	}
	return result
}

`))

var footerTemplate = template.Must(template.New("").Funcs(funcMap).Parse(`// {{ xid "accept" }}Item interprets p as a pogo.Item
func {{ xid "accept" }}Item(v {{.}}Visitor, p pogo.Parsed) pogo.Item {
	if item, ok := p.(pogo.Item); ok {
		return item
	}

	panic("expected Item")
}
`))

var lambdaVisitorFuncTemplate = template.Must(template.New("").Parse(``))

var funcMap = template.FuncMap{
	"xid": exportedIdent,
}

func exportedIdent(ident string) string {
	if *public {
		return strings.ToUpper(string(ident[0])) + ident[1:]
	}
	return ident
}

func (v *visitorTemplate) Generate(ident, pkgName, path string) string {
	var visitorIface, lambdaStruct, prodFuncs, acceptFunc, whole bytes.Buffer
	ident = exportedIdent(ident)
	fmt.Fprintf(&visitorIface, "// Code generated by pogo DO NOT EDIT\n")
	fmt.Fprintf(&visitorIface, "package %v\n\n", pkgName)
	fmt.Fprintf(&visitorIface, "var parseDebug bool\n")
	fmt.Fprintf(&visitorIface, "func init() {\n")
	fmt.Fprintf(&visitorIface, "parseDebug = os.Getenv(\"POGO_PARSE_DEBUG\") != \"\"\n")
	fmt.Fprintf(&visitorIface, "}\n")
	fmt.Fprintf(&visitorIface, "type %vVisitor interface{\n", ident)
	if v.EmbedInterface != "" {
		fmt.Fprintf(&visitorIface, "%v\n", v.EmbedInterface)
	}
	fmt.Fprintf(&lambdaStruct, "type %vLambdaVisitor struct{\n", ident)
	if v.EmbedInterface != "" {
		fmt.Fprintf(&lambdaStruct, "%v\n", v.EmbedInterface)
	}
	fmt.Fprintf(&lambdaStruct, "base %vVisitor\n", ident)

	fmt.Fprintf(&acceptFunc, "// accept interprets p as a production and accepts it\n")
	fmt.Fprintf(&acceptFunc, "func %v(v %vVisitor, p pogo.Parsed) interface{} {\n", exportedIdent("accept"), ident)
	fmt.Fprintf(&acceptFunc, "prod := p.(pogo.Production)\n")
	fmt.Fprintf(&acceptFunc, "switch prod.Ident {\n")

	for _, prod := range v.ProductionOrder {
		typ := v.Productions[prod]
		var typeName string
		if typ != nil {
			typeName = fmt.Sprintf("%s", typ)
			typePackageName := packageShortName(typ.PkgPath())
			if typePackageName == pkgName {
				typeName = typ.Name()
			}
		}

		tmplData := struct {
			Prod           string
			TypeName       string
			PkgName        string
			EmbedInterface string
		}{
			Prod:           prod,
			TypeName:       typeName,
			PkgName:        ident,
			EmbedInterface: v.EmbedInterface,
		}

		if typ != nil {
			fmt.Fprintf(&visitorIface, "%v%v(delegate %vVisitor, items []pogo.Parsed) %v\n", exportedIdent("visit"), prod, ident, typeName)
			fmt.Fprintf(&lambdaStruct, "%v%vFunc func(%vVisitor, []pogo.Parsed) %v\n", exportedIdent("visit"), prod, ident, typeName)
			prodTemplate.Execute(&prodFuncs, tmplData)

			fmt.Fprintf(&acceptFunc, "case \"%v\":\n", prod)
			fmt.Fprintf(&acceptFunc, "return %v%v(v, p)\n", exportedIdent("accept"), prod)
		} else {
			fmt.Fprintf(&visitorIface, "%v%v(delegate %vVisitor, items []pogo.Parsed)\n", exportedIdent("visit"), prod, ident)
			fmt.Fprintf(&lambdaStruct, "%v%vFunc func(%vVisitor, []pogo.Parsed)\n", exportedIdent("visit"), prod, ident)

			untypedProdTemplate.Execute(&prodFuncs, tmplData)

			fmt.Fprintf(&acceptFunc, "case \"%v\":\n", prod)
			fmt.Fprintf(&acceptFunc, "%v%v(v, p)\n", exportedIdent("accept"), prod)
			fmt.Fprintf(&acceptFunc, "return nil\n")
		}

		commonProdTemplate.Execute(&prodFuncs, tmplData)
		lambdaVisitorFuncTemplate.Execute(&prodFuncs, tmplData)
	}
	fmt.Fprintf(&visitorIface, "}\n\n")
	fmt.Fprintf(&lambdaStruct, "}\n")

	fmt.Fprintf(&acceptFunc, "default:\n")
	fmt.Fprintf(&acceptFunc, "panic(\"unknown production\")\n")
	fmt.Fprintf(&acceptFunc, "}\n")
	fmt.Fprintf(&acceptFunc, "}\n")

	fmt.Fprintf(&whole, visitorIface.String())
	fmt.Fprintf(&whole, lambdaStruct.String())
	fmt.Fprintf(&whole, prodFuncs.String())
	fmt.Fprintf(&whole, acceptFunc.String())
	footerTemplate.Execute(&whole, ident)

	srcName := ident + ".po.go"
	src, err := imports.Process(srcName, whole.Bytes(), nil)
	if err != nil {
		panic(err)
	}

	ioutil.WriteFile(path+"/"+srcName, src, 0744)

	return string(src)
}
