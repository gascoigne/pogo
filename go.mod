module gitlab.com/gascoigne/pogo

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	golang.org/x/tools v0.1.3
)
